from umbral import (
    signing, config,
    pre, keys
)
from umbral.fragments import KFrag
from umbral.params import UmbralParameters
from umbral.curve import SECP256K1

from users.tests.base import APITestCase
from re_encryption.models import RecordsSet
from users.models import Patient
from re_encryption.models import (
    Delegation,
    KeyFragment
)


class RecepientDataSendingCase(APITestCase):
    setup_login_patient = False

    def test_patient_decipher_without_reencryption(self):
        #Generate keys
        config.set_default_curve(SECP256K1)

        patient_private_key = keys.UmbralPrivateKey.gen_key()
        patient_public_key = patient_private_key.get_pubkey()
        recepient_private_key = keys.UmbralPrivateKey.gen_key()
        recepient_public_key = recepient_private_key.get_pubkey()

        recepient_signing_key = keys.UmbralPrivateKey.gen_key()
        recepient_verifying_key = recepient_signing_key.get_pubkey()
        recepient_signer = signing.Signer(private_key=recepient_signing_key)

        #Convert keys to hex
        hexed_patient_public_key = patient_public_key.to_bytes().hex()
        hexed_recepient_public_key = recepient_public_key.to_bytes().hex()
        hexed_recepient_verifying_key = recepient_verifying_key.to_bytes().hex()

        #Signup patient
        data = {
            'first_name': 'Gordo',
            'last_name': 'Freema',
            'email': 'patient@example.com',
            'password': 'q123sldj23',
            'eth_address': '0x73015966604928A312F79F7E69291a656Cb88603',
            'pub_key': hexed_patient_public_key
        }

        response = self.client.post('/patients/signup/', data)

        #Signup recepient
        data = {
            'organisation_id': 'someseriousorganisationidentifier',
            'first_name': 'Gordon',
            'last_name': 'Freeman',
            'email': 'recepient@example.com',
            'password': 'q123sldj23',
            'eth_address': '0x73015966604928A312F79F7E69291a656Cb88603',
            'pub_key': hexed_recepient_public_key
        }

        response = self.client.post('/recepients/signup/', data)

        #Log in as patient
        self.client.login(username='patient@example.com', password='q123sldj23')

        data = {
            'patient_id': 2,
            'recepient_id': 2
        }

        #Delegate add records permission to recepient
        response = self.client.post('/patients/delegations/make/', data)

        #Log in as recepient
        self.client.login(username='recepient@example.com', password='q123sldj23')

        #Encrypt message
        plaintext = b'Proxy Re-encryption is cool!'
        ciphertext, capsule = pre.encrypt(patient_public_key, plaintext)

        #Add encrypted record
        data = {
            'patient_id': 2,
            'type': 'type',
            'data': ciphertext.hex(),
            'capsule': capsule.to_bytes().hex()
        }

        response = self.client.post('/records/add/', data)

        self.assertEqual(response.status_code, 201)
        
        #Login as patient
        self.client.login(username='patient@example.com', password='q123sldj23')

        response = self.client.get('/me/records/')

        #Get data organisation added
        from_proxy_capsule = bytes.fromhex(response.data[0]['capsule'])
        from_proxy_data = bytes.fromhex(response.data[0]['data'])

        umbral_parameteres = UmbralParameters(SECP256K1)

        from_proxy_capsule = pre.Capsule.from_bytes(
            from_proxy_capsule,
            umbral_parameteres
        )

        #Decrypt with patient`s private key
        cleartext = pre.decrypt(
            ciphertext=from_proxy_data,
            capsule=from_proxy_capsule,
            decrypting_key=patient_private_key
        )

        self.assertEqual(cleartext, plaintext)
